import Ipopt

struct SimpleGame
    I # agents
    A # joint action space
    R # joint reward function
end

struct IteratedBestResponse
    k_max # number of iterations
    π # initial policy
end

function IteratedBestResponse(P::SimpleGame, k_max)
    π = [rand(Ai) for Ai in P.A]
    return IteratedBestResponse(k_max, π)
end 

joint(π, πi, i) = [i == j ? πi : πj for (j, πj) in enumerate(π)]

function best_response(P::SimpleGame, π, i)
    U(ai) = R(joint(π, ai, i))[i]
    ai = argmax(U, P.A[i])
    return ai
end

function solve(M::IteratedBestResponse, P)
    π = M.π
    for k in 1:M.k_max
        @show π
        π = [best_response(P, π, i) for i in P.I]
    end
    return π    
end

k_max = 5
# I = [1, 2] # agents

# battle of the sexes
# w = 3
# b1 = 2
# b2 = -1
# A = [[1, -1], [1, -1]] # joint action space
# R(a) = [b1*a[1]+w*a[1]*a[2], b2*a[2]+w*a[1]*a[2]] # joint reward function -2-3, 

# Bach and Stravinsky
# A = [["Bach", "Stravinsky"], ["Bach", "Stravinsky"]] # joint action space
# r1 = Dict("Bach"=> Dict("Bach" => 2, "Stravinsky" => 0), "Stravinsky"=> Dict("Bach" => 0, "Stravinsky" => 1))
# r2 = Dict("Bach"=> Dict("Bach" => 1, "Stravinsky" => 0), "Stravinsky"=> Dict("Bach" => 0, "Stravinsky" => 2))
# R(a) = [r1[a[1]][a[2]], r2[a[1]][a[2]]] # joint reward function

# Graph
# NE: 1,1,1,-1 or -1,-1,-1,1
# NE: 1,-1,-1,1 or -1,1,1,-1 1=
I = [1, 2, 3, 4]
A = [[-1, 1], [-1, 1], [-1, 1], [-1, 1]]
weight = Dict(1 => Dict(2 => 3, 3 => 6, 4 => -5), 
              2 => Dict(1 => 3, 3 => 0, 4 => -2), 
              3 => Dict(1 => 6, 2 => 0, 4 => -2), 
              4 => Dict(1 => -5, 2 => -2, 3 => -2))
r1(a) = sum(weight[1][j]*a[1]*a[j] for j in [2,3,4])
r2(a) = sum(weight[2][j]*a[2]*a[j] for j in [1,3,4])
r3(a) = sum(weight[3][j]*a[3]*a[j] for j in [1,2,4])
r4(a) = sum(weight[4][j]*a[4]*a[j] for j in [1,2,3])
R(a) = [r1(a), r2(a), r3(a), r4(a)]

game = SimpleGame(I, A, R)
M = IteratedBestResponse(game, k_max)
π = solve(M, game)
