using JuMP 
import Ipopt
include("bimatrix.jl")

function create_payoff_matrices(X::Vector, Y::Vector, f1, f2; optimum=MOI.MAX_SENSE)
    A = [f1(x, y) for x in X, y in Y]
    B = [f2(x, y) for x in X, y in Y]

    if optimum == MOI.MAX_SENSE
        A, B
    else
        -A, -B
    end
end 

function display_strategy(strategy)
    for (prob, sup) in strategy
        if isapprox(prob, 0.0)
            continue
        end
        sup_str = round(sup; digits=3)
        prob_str = round(prob * 100; digits=3)
        println("\t $sup_str with prob $prob_str%")
    end
end

# oracle
function oracle(model, x, f1, f2; optimum=MOI.MAX_SENSE, k_max=10)
    for j in 1:k_max
        # find pure strategy for each player and save it
        # for player one
        @NLobjective(model, optimum, sum(f1(x, value) * probability for (probability, value) in strategies[2]))
        optimize!(model)
        union!(pure_1, value(x))
        instability_1[j] = sum(f1(value(x), v2) * probability for (probability, v2) in strategies[2]) -
                        sum(sum(f1(v, v2) * probability for (probability, v2) in strategies[2]) * p for (p, v) in strategies[1])
        # for player two
        @NLobjective(model, optimum, sum(f2(value, x) * probability for (probability, value) in strategies[1]))
        optimize!(model)
        union!(pure_2, value(x))
        instability_2[j] = sum(f2(v1, value(x)) * probability for (probability, v1) in strategies[1]) -
                        sum(sum(f2(v1, v) * probability for (probability, v1) in strategies[1]) * p for (p, v) in strategies[2])

        payoff_A, payoff_B = create_payoff_matrices(pure_1, pure_2, f1, f2, optimum=optimum)
        # find mixed strategies 
        _, s = nash_equilibrium(payoff_A, payoff_B) # s[i] - mixed strategy P_i
        strategies[1] = zip(s[1], pure_1) # zip(P_1, X_1)
        strategies[2] = zip(s[2], pure_2) # zip(P_2, X_2)
    end

    strategies
end
