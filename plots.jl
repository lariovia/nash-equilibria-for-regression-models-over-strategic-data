using PyCall

py"""
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22, 'figure.figsize': (9,7)})
x = range(1,11)

def cournot_oligopoly(instability_1, instability_2):
    plt.plot(x, instability_1, '-bs')
    plt.plot(x, instability_2, '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Instability')
    plt.legend(['Player 1', 'Player 2'])
    plt.savefig('CournotOligopoly.pdf', bbox_inches='tight')

def torus(instability_1, instability_2):
    plt.plot(x, instability_1, '-bs')
    plt.plot(x, instability_2, '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Instability')
    plt.legend(['Player 1', 'Player 2'])
    plt.savefig('Torus.pdf', bbox_inches='tight')

def double_oracle_example3(instability_1, instability_2):
    plt.figure(figsize=(11,8))
    plt.plot(x, instability_1, '-bs')
    plt.plot(x, instability_2, '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Instability')
    plt.legend(['Player 1', 'Player 2'])
    plt.savefig('DOMG.pdf', bbox_inches='tight')

def IBR_example1(diff_1, diff_2):
    plt.figure(figsize=(11,8))
    plt.plot(x, diff_1, '-bs')
    plt.plot(x, diff_2, '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Change in the profit')
    plt.legend(['Player 1', 'Player 2'])
    plt.savefig('IBR1.pdf', bbox_inches='tight')

def IBR_example2(strategies):
    plt.figure(figsize=(11,8))
    plt.plot(x, strategies[0], '-bs')
    plt.plot(x, strategies[1], '-gs')
    plt.plot(x, strategies[2], '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Precision at equilibrium')
    plt.legend(['Player 1', 'Player 2', 'Player 3'])
    plt.savefig('IBR2.pdf', bbox_inches='tight')

def IBR_example3(strategies):
    plt.figure(figsize=(11,8))
    plt.plot(x, strategies[0], '-bs')
    plt.plot(x, strategies[1], '-gs')
    plt.plot(x, strategies[2], '-cs')
    plt.plot(x, strategies[3], '-ms')
    plt.plot(x, strategies[4], '-ys')
    plt.plot(x, strategies[5], '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Precision at equilibrium')
    plt.legend(['Player 1', 'Player 2', 'Player 3', 
                'Player 4', 'Player 5', 'Player 6'])
    plt.savefig('IBR3.pdf', bbox_inches='tight')

def optimum_1(policy, delta_max, estimations):
    deltas = np.linspace(0, delta_max, 100)
    plt.figure(figsize=(20, 8))
    plt.subplot(1, 2, 1)
    plt.plot(deltas, estimations)
    plt.ylabel('Estimation cost at equilibrium')
    plt.xlabel('Perturbation $\delta$')

    plt.subplot(1, 2, 2)
    plt.plot(deltas, policy[0])
    plt.plot(deltas, policy[1], '--')
    plt.xlabel('Perturbation $\delta$')
    plt.ylabel('Precision at equilibrium')
    plt.legend(['Precision of Agent 1', 'Precision of Agent 2'])
    plt.savefig('optimumExample1.pdf', bbox_inches='tight')

def optimum_2(delta, estimations):
    plt.figure(figsize=(12,8))
    deltas = np.linspace(0, delta, 100)
    plt.plot(deltas, estimations)
    plt.ylabel('Estimation cost at equilibrium')
    plt.xlabel('Perturbation $\delta$')

    plt.savefig('optimumExample22.pdf', bbox_inches='tight')

def bayes(player1, player2):
    plt.figure(figsize=(11,8))
    p1 = plt.plot(x, player1, '-bs')
    p2 = plt.plot(x, player2, '-ro')
    plt.xlabel('Iteration')
    plt.ylabel('Strategy')
    plt.legend([p1[1], p2[1]], ['Player 1', 'Player 2'], loc=4)
    plt.savefig('bayes.pdf', bbox_inches='tight')
"""

function plot_torus(instability_1, instability_2)
    py"torus"(instability_1, instability_2)
end

function plot_cournot_oligopoly(instability_1, instability_2)
    py"cournot_oligopoly"(instability_1, instability_2)
end

function plot_potential_game(instability_1, instability_2)
    py"double_oracle_example3"(instability_1, instability_2)
end

function plot_IBR_example1(iterations)
    diff_1 = []
    diff_2 = []
    for i in range(2,length(iterations))
        diff = iterations[i-1] - iterations[i]
        append!(diff_1, diff[1]) 
        append!(diff_2, diff[2])
    end 
    py"IBR_example1"(diff_1, diff_2)
end

function plot_IBR_example23(iterations)
    strategies = [[] for _ in iterations[1]]
    for iter in iterations[1:end-1]
        for (idx, s) in enumerate(iter)
            append!(strategies[idx], s)
        end
    end
    if length(iterations[1]) == 3
        py"IBR_example2"(strategies)
    else
        py"IBR_example3"(strategies)
    end
end

function plot_optimum_1(policy, delta_max, estimations)
    py"optimum_1"(policy, delta_max, estimations)
end

function plot_optimum_2(delta, estimations)
    py"optimum_2"(delta, estimations)
end

function plot_bayes(iterations)
    player1 = [[] for _ in range(1,10)]
    player2 = [[] for _ in range(1,10)]
    for (idx, iteration) in enumerate(iterations) 
        player1[idx] = iteration[1]
        player2[idx] = iteration[2]
    end
    py"bayes"(player1, player2)
end