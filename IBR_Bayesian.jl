include("plots.jl")
include("IBR_continious.jl")

ϴ = [1//4, 2//4, 3//4, 1]
p = 1 / length(ϴ)^2

#init payoff functions
δ = 0.01
c1(λ) = λ^1.01
c2(λ) = λ^20
f(Λ) = 1/(Λ[1] + Λ[2]) + δ/Λ[1] + δ/Λ[2]
J1(λ_1, λ_2, θ_1, θ_2) = c1(λ_1) + f(*(Diagonal([θ_1, θ_2]), [λ_1, λ_2]))
J2(λ_1, λ_2, θ_1, θ_2) = c2(λ_2) + f(*(Diagonal([θ_1, θ_2]), [λ_1, λ_2]))

function best_response(π, i)
    s = zeros(length(ϴ))
    if i == 1
        for (index, θ_1) in enumerate(ϴ)
            @NLobjective(model, Min, sum(J1(x, π[2][idx], θ_1, θ_2)*p for (idx, θ_2) in enumerate(ϴ)))
            optimize!(model)
            s[index] = value(x)
        end
    elseif i == 2
        for (index, θ_2) in enumerate(ϴ)
            @NLobjective(model, Min, sum(J2(π[1][idx], x, θ_1, θ_2)*p for (idx, θ_1) in enumerate(ϴ)))
            optimize!(model)
            s[index] = value(x)
        end
    end
    return s
end   

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :J1, 4, J1; autodiff=true)
register(model, :J2, 4, J2; autodiff=true)
@variable(model, 1 >= x >= 0.001)
set_silent(model)

# init game
k_max = 10
agents = [1, 2]
game = SimpleGame(agents, ϴ)
M = IteratedBestResponse(game, k_max)
iterations = [[M.π[1], M.π[2]]]
π = solve(M, game)
plot_bayes(iterations[1:end-1])