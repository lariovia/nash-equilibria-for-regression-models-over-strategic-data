include("plots.jl")
include("IBR_continious.jl")

d = 2
δ = 0.001
X = create_X(d)
D = perturbation_matrix(d, δ)

#init payoff functions
c1(λ) = λ^20
c2(λ) = λ^1.5
f(λ_1, λ_2) = 1/(λ_1 + λ_2) + δ/λ_1 + δ/λ_2
J1(λ_1, λ_2, λ_3) = c1(λ_1) + estimation_cost([λ_1, λ_2, λ_3], X, D)
J2(λ_1, λ_2, λ_3) = c1(λ_2) + estimation_cost([λ_1, λ_2, λ_3], X, D)
J3(λ_1, λ_2, λ_3) = c2(λ_3) + estimation_cost([λ_1, λ_2, λ_3], X, D)

function best_response(π, i)
    if i == 1
        @NLobjective(model, Min, J1(x, π[2][1], π[3][1]))
    elseif i == 2
        @NLobjective(model, Min, J2(π[1][1], x, π[3][1]))
    elseif i == 3 
        @NLobjective(model, Min, J3(π[1][1], π[2][1], x))
    end
    optimize!(model)
    ai = value(x)
    return ai
end   

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :J1, 3, J1; autodiff=true)
register(model, :J2, 3, J2; autodiff=true)
register(model, :J3, 3, J3; autodiff=true)
@variable(model, 1 >= x >= 0.000001)
set_silent(model)

# init game
k_max = 10
ϴ = [1] # types
agents = [1, 2, 3] 
game = SimpleGame(agents, ϴ)
M = IteratedBestResponse(game, k_max)
iterations = [[M.π[1][1], M.π[2][1], M.π[3][1]]]
π = solve(M, game)
plot_IBR_example23(iterations)
