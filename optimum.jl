using JuMP
import Ipopt
using LinearAlgebra
include("IBR_continious.jl")
include("plots.jl")

function estimation_cost(Λ, X, D)
    # f(λ) = F(V(λ)), F=trace
    diag_Λ = Diagonal(Λ)
    GLS_cost = tr(inv(*(transpose(X), *(diag_Λ, X))))
    D_cost = sum(d^2/λ for (d, λ) in zip(D, Λ))

    return GLS_cost + D_cost
end

function potential(x...)
    Λ = vec([λ for λ in x])

    estimation = estimation_cost(Λ, X, D)
    individual_cost = sum(λ^exponent for (λ, exponent) in zip(Λ, exponents))
    
    return estimation + individual_cost
end

function find_NE(δ, dim)
    global D = perturbation_matrix(dim, δ)
    @NLobjective(model, Min, potential(x...))
    optimize!(model)
    x, estimation_cost([value(λ) for λ in x], X, D)
end

# init game 
# Example 1
# δ = 0.01
# num_agent = 2
# X = [1, 1]
# D = [1, -1] * sqrt(δ)
# exponents = [1.01, 20]

# Example 2
# dim = 2
# δ = 0.001
# num_agent = dim + 1
# X = create_X(dim)
# D = perturbation_matrix(dim, δ)
# exponents = [20, 20, 1.5]

# Example 3
dim = 5
δ = 0.00001
num_agent = dim + 1
X = create_X(dim)
D = perturbation_matrix(dim, δ)
exponents = [20, 20, 20, 20, 20, 1.5]

# Example 4
# dim = 10
# δ = 0.0000001
# num_agent = dim + 1
# X = create_X(dim)
# D = perturbation_matrix(dim, δ)
# exponents = vcat(ones(dim) * 20, 1.5)

# init model
model = Model(Ipopt.Optimizer)
set_silent(model)
register(model, :potential, num_agent, potential; autodiff = true)
@variable(model, 1 >= x[1:num_agent] >= 0.01)

# Example 1
# policy = [[] for _ in [1,num_agent]]
# estimations = []

# for δ in range(0, stop=0.06, length=100)
#     ne, estim = find_NE(δ, [1, -1])
#     push!(estimations, estim)
#     for (idx, v) in enumerate(ne)
#         policy[idx] = vcat(policy[idx], value(v))
#     end
# end

# plot_optimum_1(policy, 0.06, estimations)

# Example 2
estimations = []

for δ in range(0, stop=0.000025, length=100)
    ne, estim = find_NE(δ, dim)
    push!(estimations, estim)
end

plot_optimum_2(0.000025, estimations)
