include("plots.jl")
include("IBR_continious.jl")

#init payoff functions
δ = 0.01
c1(λ) = λ^1.01
c2(λ) = λ^20
f(λ_1, λ_2) = 1/(λ_1 + λ_2) + δ/λ_1 + δ/λ_2
J1(λ_1, λ_2) = c1(λ_1) + f(λ_1, λ_2)
J2(λ_1, λ_2) = c2(λ_2) + f(λ_1, λ_2)

function best_response(π, i)
    if i == 1
        @NLobjective(model, Min, J1(x, π[2][1]))
    else 
        @NLobjective(model, Min, J2(π[1][1], x))
    end
    optimize!(model)
    ai = value(x)
    return ai
end   

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :J1, 2, J1; autodiff=true)
register(model, :J2, 2, J2; autodiff=true)
@variable(model, 1 >= x >= 0)
set_silent(model)

k_max = 10
ϴ = [1] # types
I = [1, 2] # agents
game = SimpleGame(I, ϴ)
M = IteratedBestResponse(game, k_max)
iterations = [[M.π[1][1], M.π[2][1]]]
π = solve(M, game)
plot_IBR_example1(iterations)