using JuMP 
import Ipopt
using LinearAlgebra

struct SimpleGame
    I # agents
    ϴ # types
end

struct IteratedBestResponse
    k_max # number of iterations
    π # initial policy
end

function IteratedBestResponse(G::SimpleGame, k_max)
    π = [rand(length(G.ϴ)) for _ in G.I]
    return IteratedBestResponse(k_max, π)
end 

function solve(M::IteratedBestResponse, G)
    π = M.π
    for k in 1:M.k_max
        π = [best_response(π, i) for i in G.I]
        append!(iterations, [π])
        @show π
    end
    return π    
end

function estimation_cost(Λ, X, D)
    # f(λ) = F(V(λ)), F=trace
    diag_Λ = Diagonal(Λ)
    GLS_cost = tr(inv(*(transpose(X), *(diag_Λ, X))))
    D_cost = tr(*(transpose(D), *(inv(diag_Λ), D)))
    
    return GLS_cost + D_cost
end

function create_X(dim)
    X = vcat(I(dim), ones((1, dim)) / dim)
    return X 
end

function perturbation_matrix(dim, δ)
    v = ones(dim+1) * sqrt(δ)
    v[end] *= -dim
    D = zeros(dim+1, dim)
    D[:, 1] = v
    return D
end
