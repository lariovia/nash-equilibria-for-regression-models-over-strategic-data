include("plots.jl")
include("IBR_continious.jl")

d = 5
δ = 0.00001
X = create_X(d)
D = perturbation_matrix(d, δ)

#init payoff functions
c1(λ) = λ^20
c2(λ) = λ^1.5
f(λ_1, λ_2) = 1/(λ_1 + λ_2) + δ/λ_1 + δ/λ_2
J1(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c1(λ_1) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)
J2(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c1(λ_2) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)
J3(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c1(λ_3) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)
J4(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c1(λ_4) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)
J5(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c1(λ_5) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)
J6(λ_1, λ_2, λ_3, λ_4, λ_5, λ_6) = c2(λ_6) + estimation_cost([λ_1, λ_2, λ_3, λ_4, λ_5, λ_6], X, D)

function best_response(π, i)
    if i == 1
        @NLobjective(model, Min, J1(x, π[2][1], π[3][1], π[4][1], π[5][1], π[6][1]))
    elseif i == 2
        @NLobjective(model, Min, J2(π[1][1], x, π[3][1], π[4][1], π[5][1], π[6][1]))
    elseif i == 3 
        @NLobjective(model, Min, J3(π[1][1], π[2][1], x, π[4][1], π[5][1], π[6][1]))
    elseif i == 4
        @NLobjective(model, Min, J4(π[1][1], π[2][1], π[3][1], x, π[5][1], π[6][1]))
    elseif i == 5 
        @NLobjective(model, Min, J5(π[1][1], π[2][1], π[3][1], π[4][1], x, π[6][1]))
    elseif i == 6
        @NLobjective(model, Min, J6(π[1][1], π[2][1], π[3][1], π[4][1], π[5][1], x))
    end
    optimize!(model)
    ai = value(x)
    return ai
end   

#init model Ipopt
model = Model(Ipopt.Optimizer)
register(model, :J1, 6, J1; autodiff=true)
register(model, :J2, 6, J2; autodiff=true)
register(model, :J3, 6, J3; autodiff=true)
register(model, :J4, 6, J4; autodiff=true)
register(model, :J5, 6, J5; autodiff=true)
register(model, :J6, 6, J6; autodiff=true)
@variable(model, 1 >= x >= 0.000001)
set_silent(model)

# init game
k_max = 10
ϴ = [1] # types
agents = [1,2,3,4,5,6]
game = SimpleGame(agents, ϴ)
M = IteratedBestResponse(game, k_max)
iterations = [[M.π[1][1], M.π[2][1], M.π[3][1], M.π[4][1], M.π[5][1], M.π[6][1]]]
π = solve(M, game)
plot_IBR_example23(iterations)