using JuMP 
import Ipopt
include("plots.jl")
include("oracle.jl")
include("bimatrix.jl")

# Example 1
c = 1
d = 10
function u1_1(a1, a2) 
    if a1+a2 <= d
        return a1 * (d-c-a1-a2)
    else
        return -a1 * c
    end
end
function u2_1(a1, a2)
    if a1+a2 <= d
        return a2 * (d-c-a1-a2)
    else
        return -a2 * c
    end
end

# Example 2
alpha_1 = 1.0
alpha_2 = 1.5
phi_1 = 0.0
phi_2 = pi / 8
u1_2(t1, t2) = alpha_1 * cos(t1 - phi_1) - cos(t1 - t2)
u2_2(t1, t2) = alpha_2 * cos(t2 - phi_2) - cos(t2 - t1)

# Example 3
δ = 0.01
c1(λ) = λ^1.01
c2(λ) = λ^20
f(λ_1, λ_2) = 1/(λ_1 + λ_2) + δ/λ_1 + δ/λ_2
J1(λ_1, λ_2) = c1(λ_1) + f(λ_1, λ_2)
J2(λ_1, λ_2) = c2(λ_2) + f(λ_1, λ_2)

function init_model(a, b, f1, f2)
    model = Model(Ipopt.Optimizer)
    set_silent(model)
    register(model, :f1, 2, f1; autodiff=true)
    register(model, :f2, 2, f2; autodiff=true)
    @variable(model, b >= x >= a)
    
    model, x
end 

instability_1 = zeros(10)
instability_2 = zeros(10)

# Example 1: Cournot oligopoly
# a = 1
# b = 10
# pure_1 = rand((1:2.0), 1)
# pure_2 = rand((1:2.0), 1)
# strategies = [zip([1.0], pure_1), zip([1.0], pure_2)]
# # find Nash equilibrium
# model, x = init_model(a, b, u1_1, u2_1)
# strategies = oracle(model, x, u1_1, u2_1)
# plot_cournot_oligopoly(instability_1, instability_2)

# Example 2: Torus game
# a = -pi
# b = pi
# pure_1 = rand((1:2.0), 1)
# pure_2 = rand((1:2.0), 1)
# strategies = [zip([1.0], pure_1), zip([1.0], pure_2)]
# model, x = init_model(a, b, u1_2, u2_2)
# strategies = oracle(model, x, u1_2, u2_2)
# plot_torus(instability_1, instability_2)

# Example 3: no name
a = 0
b = 1
pure_1 = rand(1)
pure_2 = rand(1)
strategies = [zip([1.0], pure_1), zip([1.0], pure_2)]
model, x = init_model(a, b, J1, J2)
strategies = oracle(model, x, J1, J2, optimum=MOI.MIN_SENSE)
plot_potential_game(instability_1, instability_2)

println("Strategy of player 1:")
display_strategy(strategies[1])
println("Strategy of player 2:")
display_strategy(strategies[2])
